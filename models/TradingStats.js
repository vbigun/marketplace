const mongoose = require('mongoose');

const TradingStatsSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
      required: true
    },
    currency: {
      type: String,
      default: 'BTC'
    },
    currencySum: {
      type: Number
    },
    fail_price: {
      type: Number
    },
    buyPrice: {
      type: Number
    },
    salePrice: {
      type: Number
    },
    status: {
      // 0 -- open; 1 -- close
      type: Boolean,
      default: 0
    },
    success: {
      //'closedWin' 'closedLose'
      type: String,
      default: 'Open'
    }
  },
  {
    timestamps: true
  }
);

const TradingStats = mongoose.model('TradeStats', TradingStatsSchema);

module.exports = TradingStats;
