const mongoose = require('mongoose');

const TradingLogSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
      required: true
    },
    buyPrice: {
      type: Number
    },
    bitcoinQuantity: {
      type: Number
    },
    currency: {
      type: String,
      default: 'BTC'
    },
    transactionType: {
      // 1 -- buy; 2 -- sell
      type: Number
    },
    sellPrice: {
      type: Number
    },
    failedPrice: {
      type: Number
    },
    date: {
      type: Date,
      default: Date.now
    }
  },
  {
    timestamps: true
  }
);

const TradeLog = mongoose.model('TradeLog', TradingLogSchema);

module.exports = TradeLog;
