const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  role: {
    type: String,
    default: 'Traider'
  },
  photo: {
    data: Buffer,
    contentType: String
  },
  phone_number: {
    type: String
  },
  city: {
    type: String
  },
  username: {
    type: String
  },
  surname: {
    type: String
  },
  userBalance: {
    type: Number,
    default: 0
  },
  leverage: {
    type: Number,
    default: 50
  },
  userBitcoin: {
    type: Number,
    default: 0
  }
});

const User = mongoose.model('User', UserSchema, 'UsersInfo');

module.exports = User;
