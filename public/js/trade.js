// console.log('hi');

// const socket = io.connect('http://demo01.megatrade.site:7721/', {
//   secure: true
// });
const socket = io.connect('http://localhost:3000');

//sdsd

const table = document.getElementById('table');

const lastPrice = document.querySelector('.lastprice');

const modalPrice = document.querySelector('.modal-price');

/*20 lines*/
const root = document.getElementById('root');

const realtimeChange = document.querySelector('.realtime-change');

const btc = document.querySelector('.btc');

const eth = document.querySelector('.eth');

const ltc = document.querySelector('.ltc');

const xrp = document.querySelector('.xrp');

const eos = document.querySelector('.eos');

const lastPricebtc = document.querySelector('.lastpricebtc');

const lastPriceEth = document.querySelector('.lastpriceeth');

const lastPriceLtc = document.querySelector('.lastpriceltc');

const lastPriceEos = document.querySelector('.lastpriceeos');

const lastPriceXrp = document.querySelector('.lastpricexrp');

const modalPriceBTC = document.querySelector('.modal-price-btc');

const modalPriceETH = document.querySelector('.modal-price-eth');

const modalPriceEOS = document.querySelector('.modal-price-eos');

const modalPriceLTC = document.querySelector('.modal-price-ltc');

const modalPriceXRP = document.querySelector('.modal-price-xrp');

const graphWrapper = document.querySelector('.graph');
/*end*/

socket.on('infoBNB', payload => {
  btc.innerHTML = `
            <td class="btc-name">${payload.name}</td>
            <td class="btc-last-price">${payload.averagePrice}</td>
            <td class="btc-24-change-btc">${payload.percentChange}%</td>
            <td class="btc-24-change-high">${payload.high}</td>
            <td class="24-change-low">${payload.low}</td>
            <td class="btc-24-volume">${payload.volume}</td> `;
  if (payload.percentChange < 0) {
    if (
      document
        .querySelector('.btc-24-change-btc')
        .classList.contains('up-change')
    ) {
      document
        .querySelector('.btc-24-change-btc')
        .classList.remove('up-change');
    }
    document.querySelector('.btc-24-change-btc').classList.add('down-change');
  } else {
    if (
      document
        .querySelector('.btc-24-change-btc')
        .classList.contains('down-change')
    ) {
      document
        .querySelector('.btc-24-change-btc')
        .classList.remove('down-change');
    }
    document.querySelector('.btc-24-change-btc').classList.add('up-change');
  }
});

socket.on('infoETH', payload => {
  eth.innerHTML = `
  <td class="btc-name">${payload.name}</td>
  <td class="btc-last-price">${payload.averagePrice}</td>
  <td class="btc-24-change-eth">${payload.percentChange}%</td>
  <td class="btc-24-change-high">${payload.high}</td>
  <td class="24-change-low">${payload.low}</td>
  <td class="btc-24-volume">${payload.volume}</td> `;
  if (payload.percentChange < 0) {
    if (
      document
        .querySelector('.btc-24-change-eth')
        .classList.contains('up-change')
    ) {
      document
        .querySelector('.btc-24-change-eth')
        .classList.remove('up-change');
    }
    document.querySelector('.btc-24-change-eth').classList.add('down-change');
  } else {
    if (
      document
        .querySelector('.btc-24-change-eth')
        .classList.contains('down-change')
    ) {
      document
        .querySelector('.btc-24-change-eth')
        .classList.remove('down-change');
    }
    document.querySelector('.btc-24-change-eth').classList.add('up-change');
  }
});

/*LTC*/
socket.on('infoLTC', payload => {
  ltc.innerHTML = `
  <td class="btc-name">${payload.name}</td>
  <td class="btc-last-price">${payload.averagePrice}</td>
  <td class="btc-24-change-ltc">${payload.percentChange}%</td>
  <td class="btc-24-change-high">${payload.high}</td>
  <td class="24-change-low">${payload.low}</td>
  <td class="btc-24-volume">${payload.volume}</td> `;
  if (payload.percentChange < 0) {
    if (
      document
        .querySelector('.btc-24-change-ltc')
        .classList.contains('up-change')
    ) {
      document
        .querySelector('.btc-24-change-ltc')
        .classList.remove('up-change');
    }
    document.querySelector('.btc-24-change-ltc').classList.add('down-change');
  } else {
    if (
      document
        .querySelector('.btc-24-change-ltc')
        .classList.contains('down-change')
    ) {
      document
        .querySelector('.btc-24-change-ltc')
        .classList.remove('down-change');
    }
    document.querySelector('.btc-24-change-ltc').classList.add('up-change');
  }
});

/*XRP*/
socket.on('infoXRP', payload => {
  xrp.innerHTML = `
  <td class="btc-name">${payload.name}</td>
  <td class="btc-last-price">${payload.averagePrice}</td>
  <td class="btc-24-change-xrp">${payload.percentChange}%</td>
  <td class="btc-24-change-high">${payload.high}</td>
  <td class="24-change-low">${payload.low}</td>
  <td class="btc-24-volume">${payload.volume}</td> `;
  if (payload.percentChange < 0) {
    if (
      document
        .querySelector('.btc-24-change-xrp')
        .classList.contains('up-change')
    ) {
      document
        .querySelector('.btc-24-change-xrp')
        .classList.remove('up-change');
    }
    document.querySelector('.btc-24-change-xrp').classList.add('down-change');
  } else {
    if (
      document
        .querySelector('.btc-24-change-xrp')
        .classList.contains('down-change')
    ) {
      document
        .querySelector('.btc-24-change-xrp')
        .classList.remove('down-change');
    }
    document.querySelector('.btc-24-change-xrp').classList.add('up-change');
  }
});

/*EOS*/
socket.on('infoEOS', payload => {
  eos.innerHTML = `
  <td class="btc-name">${payload.name}</td>
  <td class="btc-last-price">${payload.averagePrice}</td>
  <td class="btc-24-change-eos">${payload.percentChange}%</td>
  <td class="btc-24-change-high">${payload.high}</td>
  <td class="24-change-low">${payload.low}</td>
  <td class="btc-24-volume">${payload.volume}</td> `;
  if (payload.percentChange < 0) {
    if (
      document
        .querySelector('.btc-24-change-eos')
        .classList.contains('up-change')
    ) {
      document
        .querySelector('.btc-24-change-eos')
        .classList.remove('up-change');
    }
    document.querySelector('.btc-24-change-eos').classList.add('down-change');
  } else {
    if (
      document
        .querySelector('.btc-24-change-eos')
        .classList.contains('down-change')
    ) {
      document
        .querySelector('.btc-24-change-eos')
        .classList.remove('down-change');
    }
    document.querySelector('.btc-24-change-eos').classList.add('up-change');
  }
});
function changeGraph(value, color) {
  let graph = new TradingView.widget({
    autosize: true,
    symbol: value,
    interval: 'D',
    timezone: 'Etc/UTC',
    theme: color,
    style: '1',
    locale: 'en',
    toolbar_bg: '#f1f3f6',
    enable_publishing: false,
    allow_symbol_change: true,
    container_id: 'tradingview_2da15'
  });
  graph.iframe.src = graph.iframe.src;
}
const currencies = [
  'BINANCE:BTCUSDT',
  'BINANCE:ETHUSDT',
  'BINANCE:LTCUSDT',
  'BINANCE:XRPUSDT',
  'BINANCE:EOSUSDT'
];
btc.addEventListener('click', () => {
  changeGraph(currencies[0], 'Dark');
  realtimeChange.classList.remove('hide');
  graphWrapper.style.width = '75%';
  // console.log(modalPriceBTC.classList);
  if ((modalPriceBTC.classList.contains = 'hide')) {
    modalPriceBTC.classList.remove('hide');
    modalPriceEOS.classList.add('hide');
    modalPriceETH.classList.add('hide');
    modalPriceLTC.classList.add('hide');
    modalPriceXRP.classList.add('hide');
  }
  if ((lastPricebtc.classList.contains = 'hide')) {
    lastPricebtc.classList.remove('hide');
    lastPriceEth.classList.add('hide');
    lastPriceLtc.classList.add('hide');
    lastPriceEos.classList.add('hide');
    lastPriceXrp.classList.add('hide');
  }
  socket.emit('sendNameBTC', { name: 'BTCUSDT' });
});

ltc.addEventListener('click', () => {
  changeGraph(currencies[2], 'Dark');
  realtimeChange.classList.remove('hide');
  graphWrapper.style.width = '75%';
  // console.log(modalPriceBTC.classList);
  // console.log(modalPriceLTC.classList);
  if ((modalPriceLTC.classList.contains = 'hide')) {
    modalPriceLTC.classList.remove('hide');
    modalPriceETH.classList.add('hide');
    modalPriceBTC.classList.add('hide');
    modalPriceEOS.classList.add('hide');
    modalPriceXRP.classList.add('hide');
  }
  if ((lastPriceLtc.classList.contains = 'hide')) {
    lastPriceLtc.classList.remove('hide');
    lastPriceEth.classList.add('hide');
    lastPricebtc.classList.add('hide');
    lastPriceEos.classList.add('hide');
    lastPriceXrp.classList.add('hide');
  }
  socket.emit('sendNameLTC', { name: 'LTCUSDT' });
});

eth.addEventListener('click', () => {
  changeGraph(currencies[1], 'Dark');
  realtimeChange.classList.remove('hide');
  graphWrapper.style.width = '75%';
  if ((modalPriceETH.classList.contains = 'hide')) {
    modalPriceETH.classList.remove('hide');
    modalPriceEOS.classList.add('hide');
    modalPriceBTC.classList.add('hide');
    modalPriceLTC.classList.add('hide');
    modalPriceXRP.classList.add('hide');
  }
  if ((lastPriceEth.classList.contains = 'hide')) {
    lastPriceEth.classList.remove('hide');
    lastPricebtc.classList.add('hide');
    lastPriceLtc.classList.add('hide');
    lastPriceEos.classList.add('hide');
    lastPriceXrp.classList.add('hide');
  }
  socket.emit('sendNameETH', { name: 'ETHUSDT' });
});
xrp.addEventListener('click', () => {
  realtimeChange.classList.remove('hide');
  graphWrapper.style.width = '75%';
  changeGraph(currencies[3], 'Dark');
  if ((modalPriceXRP.classList.contains = 'hide')) {
    modalPriceXRP.classList.remove('hide');
    modalPriceEOS.classList.add('hide');
    modalPriceBTC.classList.add('hide');
    modalPriceLTC.classList.add('hide');
    modalPriceETH.classList.add('hide');
  }
  if ((lastPriceXrp.classList.contains = 'hide')) {
    lastPriceXrp.classList.remove('hide');
    lastPricebtc.classList.add('hide');
    lastPriceLtc.classList.add('hide');
    lastPriceEos.classList.add('hide');
    lastPriceEth.classList.add('hide');
  }
  socket.emit('sendNameXRP', { name: 'XRPUSDT' });
});
eos.addEventListener('click', () => {
  realtimeChange.classList.remove('hide');
  graphWrapper.style.width = '75%';
  changeGraph(currencies[4], 'Dark');
  if ((modalPriceEOS.classList.contains = 'hide')) {
    modalPriceEOS.classList.remove('hide');
    modalPriceETH.classList.add('hide');
    modalPriceBTC.classList.add('hide');
    modalPriceLTC.classList.add('hide');
    modalPriceXRP.classList.add('hide');
  }
  if ((lastPriceEos.classList.contains = 'hide')) {
    lastPriceEos.classList.remove('hide');
    lastPricebtc.classList.add('hide');
    lastPriceEth.classList.add('hide');
    lastPriceLtc.classList.add('hide');
    lastPriceXrp.classList.add('hide');
  }
  socket.emit('sendNameEOS', { name: 'EOSUSDT' });
});

/// anton

let arrNameBTC = [];
let arrPriceBTC = [];
let arrNameETH = [];
let arrPriceETH = [];
let arrNameLTC = [];
let arrPriceLTC = [];
let arrNameEOS = [];
let arrPriceEOS = [];
let arrNameXRP = [];
let arrPriceXRP = [];
socket.on('sendDataBTC', payload => {
  arrNameBTC.push(payload.name);
  arrPriceBTC.push(Number(payload.curPrice).toFixed(2));
  // console.log(typeof payload.curPrice);

  let newArrPriceBTC = arrPriceBTC.filter((item, pos) => {
    return arrPriceBTC.indexOf(item) == pos;
  });
  if (newArrPriceBTC.length > 20) {
    arrPriceBTC.shift();
  }
  if (arrNameBTC.length > 20) {
    arrNameBTC.shift();
  }

  for (let j = 0; j < newArrPriceBTC.length; j++) {
    modalPriceBTC.innerHTML = `${arrNameBTC[0]} - ${newArrPriceBTC[j]}$`;

    lastPricebtc.innerHTML = `
          <p class="margins price hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j]
    }$</p>
          <p class="margins price-1 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 1]
    }$</p>
          <p class="margins price-2 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 2]
    }$</p>
          <p class="margins price-3 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 3]
    }$</p>
          <p class="margins price-4 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 4]
    }$</p>
          <p class="margins price-5 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 5]
    }$</p>
          <p class="margins price-6 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 6]
    }$</p>
          <p class="margins price-7 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 7]
    }$</p>
          <p class="margins price-8 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 8]
    }$</p>
          <p class="margins price-9 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 9]
    }$</p>
          <p class="margins price-10 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 10]
    }$</p>
          <p class="margins price-11 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 11]
    }$</p>
          <p class="margins price-12 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 12]
    }$</p>
          <p class="margins price-13 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 13]
    }$</p>
          <p class="margins price-14 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 14]
    }$</p>
          <p class="margins price-15 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 15]
    }$</p>
          <p class="margins price-16 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 16]
    }$</p>
          <p class="margins price-17 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 17]
    }$</p>
          <p class="margins price-18 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 18]
    }$</p>
          <p class="margins price-19 hide">${arrNameBTC[0]} - ${
      newArrPriceBTC[j - 19]
    }$</p>
          `;

    if (newArrPriceBTC[j] !== undefined) {
      document.querySelector('.price').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 1] !== undefined) {
      document.querySelector('.price-1').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 2] !== undefined) {
      document.querySelector('.price-2').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 3] !== undefined) {
      document.querySelector('.price-3').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 4] !== undefined) {
      document.querySelector('.price-4').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 5] !== undefined) {
      document.querySelector('.price-5').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 6] !== undefined) {
      document.querySelector('.price-6').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 7] !== undefined) {
      document.querySelector('.price-7').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 8] !== undefined) {
      document.querySelector('.price-8').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 9] !== undefined) {
      document.querySelector('.price-9').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 10] !== undefined) {
      document.querySelector('.price-10').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 11] !== undefined) {
      document.querySelector('.price-11').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 12] !== undefined) {
      document.querySelector('.price-12').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 13] !== undefined) {
      document.querySelector('.price-13').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 14] !== undefined) {
      document.querySelector('.price-14').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 15] !== undefined) {
      document.querySelector('.price-15').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 16] !== undefined) {
      document.querySelector('.price-16').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 17] !== undefined) {
      document.querySelector('.price-17').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 18] !== undefined) {
      document.querySelector('.price-18').classList.remove('hide');
    }
    if (newArrPriceBTC[j - 19] !== undefined) {
      document.querySelector('.price-19').classList.remove('hide');
    }
  }
});

/*ETH */
socket.on('sendDataETH', payload => {
  // console.log(payload)

  arrNameETH.push(payload.name);
  arrPriceETH.push(Number(payload.curPrice).toFixed(2));

  let newArrPriceETH = arrPriceETH.filter((item, pos) => {
    return arrPriceETH.indexOf(item) == pos;
  });
  if (newArrPriceETH.length > 20) {
    arrPriceBTC.shift();
  }
  if (arrNameETH.length > 20) {
    arrNameETH.shift();
  }

  for (let j = 0; j < newArrPriceETH.length; j++) {
    modalPriceETH.innerHTML = `${arrNameETH[0]} - ${newArrPriceETH[j]}`;

    lastPriceEth.innerHTML = `
          <p class="margins price-${j}">${arrNameETH[0]} - ${
      newArrPriceETH[j]
    }</p>
          <p class="margins price-${j + 1}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 1]
    }</p>
          <p class="margins price-${j + 2}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 2]
    }</p>
          <p class="margins price-${j + 3}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 3]
    }</p>
          <p class="margins price-${j + 4}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 4]
    }</p>
          <p class="margins price-${j + 5}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 5]
    }</p>
          <p class="margins price-${j + 6}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 6]
    }</p>
          <p class="margins price-${j + 7}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 7]
    }</p>
          <p class="margins price-${j + 8}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 8]
    }</p>
          <p class="margins price-${j + 9}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 9]
    }</p>
          <p class="margins price-${j + 10}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 10]
    }</p>
          <p class="margins price-${j + 11}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 11]
    }</p>
          <p class="margins price-${j + 12}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 12]
    }</p>
          <p class="margins price-${j + 13}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 13]
    }</p>
          <p class="margins price-${j + 14}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 14]
    }</p>
          <p class="margins price-${j + 15}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 15]
    }</p>
          <p class="margins price-${j + 16}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 16]
    }</p>
          <p class="margins price-${j + 17}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 17]
    }</p>
          <p class="margins price-${j + 18}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 18]
    }</p>
          <p class="margins price-${j + 19}">${arrNameETH[0]} - ${
      newArrPriceETH[j - 19]
    }</p>
          `;
  }
});

socket.on('sendDataLTC', payload => {
  arrNameLTC.push(payload.name);
  arrPriceLTC.push(Number(payload.curPrice).toFixed(2));

  let newArrPriceLTC = arrPriceLTC.filter((item, pos) => {
    return arrPriceLTC.indexOf(item) == pos;
  });
  if (newArrPriceLTC.length > 20) {
    arrPriceLTC.shift();
  }
  if (arrNameLTC.length > 20) {
    arrNameLTC.shift();
  }

  for (let j = 0; j < newArrPriceLTC.length; j++) {
    modalPriceLTC.innerHTML = `${arrNameLTC[0]} - ${newArrPriceLTC[j]}`;

    lastPriceLtc.innerHTML = `
          <p class="margins price-${j}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j]
    }</p>
          <p class="margins price-${j + 1}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 1]
    }</p>
          <p class="margins price-${j + 2}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 2]
    }</p>
          <p class="margins price-${j + 3}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 3]
    }</p>
          <p class="margins price-${j + 4}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 4]
    }</p>
          <p class="margins price-${j + 5}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 5]
    }</p>
          <p class="margins price-${j + 6}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 6]
    }</p>
          <p class="margins price-${j + 7}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 7]
    }</p>
          <p class="margins price-${j + 8}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 8]
    }</p>
          <p class="margins price-${j + 9}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 9]
    }</p>
          <p class="margins price-${j + 10}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 10]
    }</p>
          <p class="margins price-${j + 11}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 11]
    }</p>
          <p class="margins price-${j + 12}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 12]
    }</p>
          <p class="margins price-${j + 13}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 13]
    }</p>
          <p class="margins price-${j + 14}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 14]
    }</p>
          <p class="margins price-${j + 15}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 15]
    }</p>
          <p class="margins price-${j + 16}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 16]
    }</p>
          <p class="margins price-${j + 17}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 17]
    }</p>
          <p class="margins price-${j + 18}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 18]
    }</p>
          <p class="margins price-${j + 19}">${arrNameLTC[0]} - ${
      newArrPriceLTC[j - 19]
    }</p>
          `;
  }
});

socket.on('sendDataEOS', payload => {
  arrNameEOS.push(payload.name);
  arrPriceEOS.push(Number(payload.curPrice).toFixed(4));

  let newArrPriceEOS = arrPriceEOS.filter((item, pos) => {
    return arrPriceEOS.indexOf(item) == pos;
  });
  if (newArrPriceEOS.length > 20) {
    arrPriceEOS.shift();
  }
  if (arrNameEOS.length > 20) {
    arrNameEOS.shift();
  }

  for (let j = 0; j < newArrPriceEOS.length; j++) {
    modalPriceEOS.innerHTML = `${arrNameEOS[0]} - ${newArrPriceEOS[j]}`;

    lastPriceEos.innerHTML = `
          <p class="margins price-${j}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j]
    }</p>
          <p class="margins price-${j + 1}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 1]
    }</p>
          <p class="margins price-${j + 2}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 2]
    }</p>
          <p class="margins price-${j + 3}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 3]
    }</p>
          <p class="margins price-${j + 4}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 4]
    }</p>
          <p class="margins price-${j + 5}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 5]
    }</p>
          <p class="margins price-${j + 6}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 6]
    }</p>
          <p class="margins price-${j + 7}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 7]
    }</p>
          <p class="margins price-${j + 8}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 8]
    }</p>
          <p class="margins price-${j + 9}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 9]
    }</p>
          <p class="margins price-${j + 10}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 10]
    }</p>
          <p class="margins price-${j + 11}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 11]
    }</p>
          <p class="margins price-${j + 12}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 12]
    }</p>
          <p class="margins price-${j + 13}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 13]
    }</p>
          <p class="margins price-${j + 14}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 14]
    }</p>
          <p class="margins price-${j + 15}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 15]
    }</p>
          <p class="margins price-${j + 16}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 16]
    }</p>
          <p class="margins price-${j + 17}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 17]
    }</p>
          <p class="margins price-${j + 18}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 18]
    }</p>
          <p class="margins price-${j + 19}">${arrNameEOS[0]} - ${
      newArrPriceEOS[j - 19]
    }</p>
          `;
  }
});

socket.on('sendDataXRP', payload => {
  arrNameXRP.push(payload.name);
  arrPriceXRP.push(Number(payload.curPrice).toFixed(5));

  let newArrPriceXRP = arrPriceXRP.filter((item, pos) => {
    return arrPriceXRP.indexOf(item) == pos;
  });
  if (newArrPriceXRP.length > 20) {
    arrPriceEOS.shift();
  }
  if (arrNameXRP.length > 20) {
    arrNameEOS.shift();
  }

  for (let j = 0; j < newArrPriceXRP.length; j++) {
    modalPriceXRP.innerHTML = `${arrNameXRP[0]} - ${newArrPriceXRP[j]}`;

    lastPriceXrp.innerHTML = `
          <p class="margins price-${j}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j]
    }</p>
          <p class="margins price-${j + 1}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 1]
    }</p>
          <p class="margins price-${j + 2}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 2]
    }</p>
          <p class="margins price-${j + 3}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 3]
    }</p>
          <p class="margins price-${j + 4}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 4]
    }</p>
          <p class="margins price-${j + 5}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 5]
    }</p>
          <p class="margins price-${j + 6}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 6]
    }</p>
          <p class="margins price-${j + 7}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 7]
    }</p>
          <p class="margins price-${j + 8}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 8]
    }</p>
          <p class="margins price-${j + 9}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 9]
    }</p>
          <p class="margins price-${j + 10}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 10]
    }</p>
          <p class="margins price-${j + 11}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 11]
    }</p>
          <p class="margins price-${j + 12}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 12]
    }</p>
          <p class="margins price-${j + 13}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 13]
    }</p>
          <p class="margins price-${j + 14}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 14]
    }</p>
          <p class="margins price-${j + 15}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 15]
    }</p>
          <p class="margins price-${j + 16}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 16]
    }</p>
          <p class="margins price-${j + 17}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 17]
    }</p>
          <p class="margins price-${j + 18}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 18]
    }</p>
          <p class="margins price-${j + 19}">${arrNameXRP[0]} - ${
      newArrPriceXRP[j - 19]
    }</p>
          `;
  }
});

//sdsd
let pPrice = document.getElementById('priceBTC');
let userCash = document.getElementById('userBalance');

socket.on('userBalance', balance => {
  userCash.innerHTML = balance;
});
socket.on('price', payload => {
  pPrice.innerHTML = payload;
});
// let userBitcoin2 = document.getElementById('userBitcoin').innerText;
// if (userBitcoin2 > 0) {
//   document.querySelector('.buyButton').setAttribute('disabled', '');
// }
// if (userBitcoin2 == 0) {
//   document.querySelector('.saleButton').setAttribute('disabled', '');
// }
$(function() {
  $('#form1').submit(function(e) {
    let p = document.getElementById('priceBTC').innerText;
    let total = Number(document.getElementById('total').innerText);
    let userBitcoin = document.getElementById('userBitcoin').innerText;
    let deposite = document.getElementById('deposit');
    let leverage = document.getElementById('leverage').innerText;
    e.preventDefault(); // prevents page reloading
    //info == то что ввел юзер
    //p == цена биткоина на момент нажатия кнопки
    //userBitcoin == количества биткоина у юзера

    let info = Number($('#m').val());
    let canUser = p * info;
    if (canUser <= total) {
      let a = 100 / Number(leverage);
      let b = (100 - a) / 100;
      let failedPrice = Number(p) * b;
      document.getElementById('failedPrice').innerText = failedPrice.toFixed(6);
      //function tracker
      let timerId = setTimeout(function tick() {
        timerId = setTimeout(tick, 600);
        let currentPrice = Number(
          document.getElementById('priceBTC').innerText
        );

        // let failedPrice = Number(p) * xPrice;
        // console.log(currentPrice, failedPrice);
        if (currentPrice <= failedPrice) {
          document.getElementById('userBitcoin').innerText = 0;
          let depositeNewData = Number(
            document.getElementById('deposit').innerText
          );
          socket.emit('failedTransaction', {
            userBalance: depositeNewData,
            userBitcoin: 0,
            currencySum: 0,
            status: 1,
            success: 'closedLose'
          });
          clearTimeout(timerId);
        }
      }, 600);

      currentBitcoin = Number(userBitcoin) + Number(info);
      total = Number(total) - Number(info * p);
      // отоброжение на странице
      document.getElementById('total').innerText = `${total.toFixed(5)}`;
      document.getElementById('userBitcoin').innerText = `${currentBitcoin}`;
      deposite = (deposite.innerText = total / Number(leverage)).toFixed(5);
      let failed_price = Number(p) - deposite;
      // сокет отравки в коллекцию юзеров
      socket.emit('user info', {
        userBalance: deposite,
        userBitcoin: currentBitcoin
      });
      // сокет отправки в коллекцию ЛоговТразакций
      socket.emit('logTransaction', {
        buyPrice: Number(p),
        bitcoinQuantity: info,
        failedPrice: failed_price
      });
      // сокет отправки в коллекцию СтатсТразакций
      socket.emit('statsTransaction', {
        buyPrice: Number(p),
        currencySum: currentBitcoin,
        fail_price: failed_price
      });
      // socket.emit('chat message', { msg: $('#m').val(), price: p });

      let last = (document.getElementById(
        'lastbuy'
      ).innerText = `Your last operation: buy ${info} coins with price ${p}`);
    } else {
      let last = (document.getElementById('lastbuy').innerText = "U can't buy");
    }
    // console.log(info);
    //function obrabotchik

    //
    return (buyPrice = Number(p));
  });
});

$(function() {
  $('#form2').submit(function(e) {
    // console.log(buyPrice);
    let leverage = Number(document.getElementById('leverage').innerText);
    e.preventDefault(); // prevents page reloading
    let info = Number(document.getElementById('userBitcoin').innerText);
    let inputSale = Number($('#m2').val());
    let currentPrice = Number(document.getElementById('priceBTC').innerText);
    let failedPrice = Number(document.getElementById('failedPrice').innerText);
    let deposite = Number(document.getElementById('deposit').innerText);
    let total = Number(document.getElementById('total').innerText);
    let koefDeposite,
      userDeposite,
      userBitcoin,
      salePrice,
      newTotal = 0;

    if (inputSale <= info) {
      document.getElementById(
        'lastsale'
      ).innerText = `Your last operation: sale ${inputSale} coins with price ${currentPrice}`;

      if (currentPrice >= buyPrice) {
        koefDeposite = (currentPrice - buyPrice) * leverage;
        salePrice = (currentPrice * inputSale) / leverage;
        userDeposite = deposite + koefDeposite + salePrice;
        userBitcoin = info - inputSale;
        newTotal = userDeposite * leverage;

        document.getElementById('deposit').innerText = userDeposite;
        document.getElementById('userBitcoin').innerText = userBitcoin;
        document.getElementById('total').innerText = newTotal;
        //
      } else if (currentPrice < buyPrice) {
        koefDeposite = (buyPrice - currentPrice) * leverage;
        salePrice = (currentPrice * inputSale) / leverage;
        userDeposite = deposite - koefDeposite + salePrice;
        if (userDeposite < 0) {
          userDeposite = 0;
        }
        userBitcoin = info - inputSale;
        newTotal = userDeposite * leverage;

        document.getElementById('deposit').innerText = userDeposite;
        document.getElementById('userBitcoin').innerText = userBitcoin;
        document.getElementById('total').innerText = newTotal;
      }

      socket.emit('saleUser', {
        userBalance: userDeposite,
        userBitcoin: userBitcoin
      });
      socket.emit('saleLog', {
        salePrice: currentPrice,
        bitcoinQuantity: inputSale
      });
      socket.emit('saleStats', {
        salePrice: currentPrice,
        currencySum: userBitcoin
      });
    } else {
      document.getElementById(
        'lastsale'
      ).innerText = `u cant sale so many coins`;
    }
    return false;
  });
});

const buyButton = document.querySelector('.buyButton');
const saleButton = document.querySelector('.saleButton');
const price = document.querySelector('.price');
const balance = document.querySelector('.balance');

let bitcoinName = 'BINANCE:BTCUSDT';
let i = 0;
if (i == 0) {
  let graph = new TradingView.widget({
    width: 1000,
    height: 850,
    autosize: true,
    symbol: bitcoinName,
    interval: 'D',
    timezone: 'Etc/UTC',
    theme: 'Dark',
    style: '1',
    locale: 'en',
    toolbar_bg: '#f1f3f6',
    enable_publishing: false,
    allow_symbol_change: true,
    container_id: 'tradingview_2da15'
  });
  realtimeChange.classList.add('hide');
  graphWrapper.style.width = '100%';

  i++;
}

// buyButton.addEventListener('click', buyButtonChangeEvent);
// saleButton.addEventListener('click', saleButtonChangeEvent);

// function buyButtonChangeEvent(e) {
//   e.preventDefault();
//   console.log(+price.value);
//   console.log(+balance.innerHTML);
//   buyButton.setAttribute('disabled', '');
//   if (saleButton.hasAttribute('disabled'))
//     saleButton.removeAttribute('disabled');
// }

// $('#sendData').click(function(e) {
//   console.log('clicked');
//   e.preventDefault();
//   $.ajax({
//     global: false,
//     type: 'POST',
//     url: '/takedata', // missing quotes
//     dataType: 'html',
//     data: {
//       name: $('#profile_name').val(),
//       price: $('#price').val()
//     },
//     success: function(result) {
//       console.log(result);
//     },
//     // error: function(request, status, error) {
//     //   serviceError();
//     // }
//   });
// });

// (function() {
//   $('form').submit(function(e) {
//     console.log(price);
//     let li = document.createElement('li');
//     e.preventDefault(); // prevents page reloading
//     // socket.emit('chat message', $('#message').val());
//     // messages.appendChild(li).append($('#message').val());
//     // let span = document.createElement('span');
//     // messages.appendChild(span).append('by ' + 'Anonymous' + ': ' + 'just now');
//     // $('#message').val('');
//     return false;
//   });
// })();

//   function saleButtonChangeEvent(e) {
//     e.preventDefault();
//     //   saleButton.setAttribute('disabled', '');
//     //   if (buyButton.hasAttribute('disabled'))
//     //     buyButton.removeAttribute('disabled');
//   }
