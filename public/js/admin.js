google.charts.load('current', { packages: ['corechart'] });
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
  let data = google.visualization.arrayToDataTable([
    ['Task', 'Hours per Day'],
    ['Ukraine', 11],
    ['Germany', 2],
    ['USA', 2],
    ['Poland', 2],
    ['Sweden', 7]
  ]);

  const options = {
    legend: 'none',
    backgroundColor: 'none',
    width: 210,
    height: 210,
    pieHole: 0.3,
    slices: {
      0: { color: '#455A64' },
      1: { color: '#FFD600' },
      2: { color: '#78909C' },
      3: { color: '#CFD8DC' },
      4: { color: '#607D8B' }
    }
  };

  const chart = new google.visualization.PieChart(
    document.getElementById('donutchart')
  );
  chart.draw(data, options);
}
