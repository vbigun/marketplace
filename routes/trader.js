const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const fs = require('fs');
const watch = require('node-watch');
const readLastLines = require('read-last-lines');

const binance = require('node-binance-api')().options({
  APIKEY: 'B6HuDIMOEuFImUwx66i55Kz4MMIUXjI1K1wTe5S3fN5koNmm3MWMOoaWxK31TxTP',
  APISECRET: 's8F3CnKzIztOERwpGQemb4pe2ZoL2zQWf087IoUERVGlosYDPG71InfQkh5GX26Y',
  useServerTime: true // If you get timestamp errors, synchronize to server time at startup
});

const middleware = require('../middleware/index');
const { isLoggedIn, isAdmin } = middleware;

const User = require('../models/User');
const TradingLog = require('../models/TradingLog');
const TradingStats = require('../models/TradingStats');

module.exports = io => {
  let router = express.Router();

  // define routes
  // io is available in this scope
  router.get('/', isLoggedIn, (req, res) => {
    io.on('connection', socket => {
      let arr = ['BTCUSDT', 'ETHUSDT', 'LTCUSDT', 'XRPSDT', 'EOSUSDTS'];

      binance.websockets.prevDay('BTCUSDT', (error, response) => {
        try {
          io.sockets.emit('infoBNB', {
            name: response.symbol,
            averagePrice: Number(response.close).toFixed(2),
            percentChange: Number(response.percentChange),
            high: Number(response.high).toFixed(2),
            low: Number(response.low).toFixed(3),
            volume: Number(response.quoteVolume).toFixed(2)
            //  eventTime: response.eventTime,
            //  priceChange: response.priceChange,
            //  percentChange: response.percentChange
          });
        } catch (err) {
          if (err) throw err;
        }

        fs.appendFileSync(
          `BTCUSDT.txt`,
          JSON.stringify({
            symbol: response.symbol,
            priceChange: response.priceChange,
            percentChange: response.percentChange,
            prevClose: response.prevClose,
            averagePrice: response.close,
            high: response.high,
            low: response.low,
            volume: response.volume
          }) + '\n'
        );
      });

      /*Read start*/

      socket.on('sendNameBTC', payload => {
        let arr = [];
        let btcName = payload.name;
        console.log(btcName);

        watch(`${btcName}.txt`, { recursive: true }, function(evt, name) {
          readLastLines.read(`${btcName}.txt`, 1).then(lines => {
            let k = JSON.parse(lines);
            // console.log(k.volume);

            io.sockets.emit('sendDataBTC', {
              name: k.symbol,
              curPrice: k.averagePrice
            });
          });
        });
      });

      socket.on('sendNameETH', payload => {
        let arr = [];
        let btcName = payload.name;
        console.log(btcName);

        watch(`${btcName}.txt`, { recursive: true }, function(evt, name) {
          readLastLines.read(`${btcName}.txt`, 1).then(lines => {
            let k = JSON.parse(lines);
            // console.log(k.volume);
            io.sockets.emit('sendDataETH', {
              name: k.symbol,
              curPrice: k.averagePrice
            });
          });
        });
      });

      socket.on('sendNameLTC', payload => {
        let arr = [];
        let btcName = payload.name;
        console.log(btcName);

        watch(`${btcName}.txt`, { recursive: true }, function(evt, name) {
          readLastLines.read(`${btcName}.txt`, 1).then(lines => {
            let k = JSON.parse(lines);
            // console.log(k.volume);
            io.sockets.emit('sendDataLTC', {
              name: k.symbol,
              curPrice: k.averagePrice
            });
          });
        });
      });

      socket.on('sendNameEOS', payload => {
        let arr = [];
        let btcName = payload.name;
        console.log(btcName);

        watch(`${btcName}.txt`, { recursive: true }, function(evt, name) {
          readLastLines.read(`${btcName}.txt`, 1).then(lines => {
            let k = JSON.parse(lines);
            // console.log(k.volume);
            io.sockets.emit('sendDataEOS', {
              name: k.symbol,
              curPrice: k.averagePrice
            });
          });
        });
      });

      socket.on('sendNameXRP', payload => {
        let arr = [];
        let btcName = payload.name;
        console.log(btcName);

        watch(`${btcName}.txt`, { recursive: true }, function(evt, name) {
          readLastLines.read(`${btcName}.txt`, 1).then(lines => {
            let k = JSON.parse(lines);
            // console.log(k.volume);
            io.sockets.emit('sendDataXRP', {
              name: k.symbol,
              curPrice: k.averagePrice
            });
          });
        });
      });

      /*end */

      binance.websockets.prevDay('ETHUSDT', (error, response) => {
        try {
          io.sockets.emit('infoETH', {
            name: response.symbol,
            averagePrice: Number(response.close).toFixed(2),
            percentChange: Number(response.percentChange),
            high: Number(response.high).toFixed(2),
            low: Number(response.low).toFixed(2),
            volume: Number(response.quoteVolume).toFixed(3)
            //  eventTime: response.eventTime,
            //  priceChange: response.priceChange,
            //  percentChange: response.percentChange
          });
        } catch (err) {
          if (err) throw err;
        }

        fs.appendFileSync(
          `ETHUSDT.txt`,
          JSON.stringify({
            symbol: response.symbol,
            priceChange: response.priceChange,
            percentChange: response.percentChange,
            prevClose: response.prevClose,
            averagePrice: response.close,
            high: response.high,
            low: response.low,
            volume: response.volume
          }) + '\n'
        );
      });

      binance.websockets.prevDay('LTCUSDT', (error, response) => {
        try {
          io.sockets.emit('infoLTC', {
            name: response.symbol,
            averagePrice: Number(response.close).toFixed(2),
            percentChange: Number(response.percentChange),
            high: Number(response.high).toFixed(2),
            low: Number(response.low).toFixed(2),
            volume: Number(response.quoteVolume).toFixed(3)
            //  eventTime: response.eventTime,
            //  priceChange: response.priceChange,
            //  percentChange: response.percentChange
          });
        } catch (err) {
          if (err) throw err;
        }

        fs.appendFileSync(
          `LTCUSDT.txt`,
          JSON.stringify({
            symbol: response.symbol,
            priceChange: response.priceChange,
            percentChange: response.percentChange,
            prevClose: response.prevClose,
            averagePrice: response.close,
            high: response.high,
            low: response.low,
            volume: response.volume
          }) + '\n'
        );
      });

      binance.websockets.prevDay('XRPUSDT', (error, response) => {
        try {
          io.sockets.emit('infoXRP', {
            name: response.symbol,
            averagePrice: Number(response.close).toFixed(5),
            percentChange: Number(response.percentChange),
            high: Number(response.high).toFixed(5),
            low: Number(response.low).toFixed(5),
            volume: Number(response.quoteVolume).toFixed(3)
            //  eventTime: response.eventTime,
            //  priceChange: response.priceChange,
            //  percentChange: response.percentChange
          });
        } catch (err) {
          if (err) throw err;
        }

        fs.appendFileSync(
          `XRPUSDT.txt`,
          JSON.stringify({
            symbol: response.symbol,
            priceChange: response.priceChange,
            percentChange: response.percentChange,
            prevClose: response.prevClose,
            averagePrice: response.close,
            high: response.high,
            low: response.low,
            volume: response.volume
          }) + '\n'
        );
      });

      binance.websockets.prevDay('EOSUSDT', (error, response) => {
        try {
          io.sockets.emit('infoEOS', {
            name: response.symbol,
            averagePrice: Number(response.close).toFixed(5),
            percentChange: Number(response.percentChange),
            high: Number(response.high).toFixed(5),
            low: Number(response.low).toFixed(5),
            volume: Number(response.quoteVolume).toFixed(3)
            //  eventTime: response.eventTime,
            //  priceChange: response.priceChange,
            //  percentChange: response.percentChange
          });
        } catch (err) {
          if (err) throw err;
        }

        fs.appendFileSync(
          `EOSUSDT.txt`,
          JSON.stringify({
            symbol: response.symbol,
            priceChange: response.priceChange,
            percentChange: response.percentChange,
            prevClose: response.prevClose,
            averagePrice: response.close,
            high: response.high,
            low: response.low,
            volume: response.volume
          }) + '\n'
        );
      });

      // User.findById(req.session.passport.user)
      //   .then(console.log(user))
      //   .catch(console.log(err));

      //test update User Info

      socket.on('user info', function(userData) {
        const { userBalance, userBitcoin } = userData;
        User.findOneAndUpdate(
          {
            _id: req.session.passport.user
          },
          {
            userBalance,
            userBitcoin
          },
          { new: true },
          (err, lists) => {
            if (!err) {
              console.log('all good save user collection updated');
            } else {
              console.log(err);
            }
          }
        );
      });
      // socket with LogTransaction
      socket.on('logTransaction', transactionData => {
        const { buyPrice, bitcoinQuantity, failedPrice } = transactionData;
        const newTransactionLog = new TradingLog({
          userId: req.session.passport.user,
          //dobavit' user name suda
          buyPrice: buyPrice,
          bitcoinQuantity: bitcoinQuantity,
          transactionType: 1,
          failedPrice: failedPrice
        });
        newTransactionLog
          .save()
          .then(() => {
            console.log('success save transactionLog');
          })
          .catch(err => console.log(err));
      });
      //socket with StatsTransaction
      socket.on('statsTransaction', transactionStatsData => {
        const { buyPrice, currencySum, fail_price } = transactionStatsData;
        const newTransactionStats = new TradingStats({
          userId: req.session.passport.user,
          buyPrice: buyPrice,
          currencySum: currencySum,
          fail_price: fail_price
        });

        TradingStats.findOne(
          {
            userId: req.session.passport.user,
            status: 0
          },
          (err, trading) => {
            if (trading === null) {
              newTransactionStats
                .save()
                .then(() => {
                  console.log('success save transactionStats not null');
                })
                .catch(err => console.log(err));
            } else
              console.log(
                'null TradingStats, dont create because have open stats'
              ); // Если создана не создаем еще
          }
        );
      });

      // socket dlya failed trransaction
      socket.on('failedTransaction', loseData => {
        const {
          userBalance,
          userBitcoin,
          currencySum,
          status,
          success
        } = loseData;

        User.findOneAndUpdate(
          {
            _id: req.session.passport.user
          },
          {
            userBalance,
            userBitcoin
          },
          { new: true },
          (err, lists) => {
            if (!err) {
              console.log('lose transaction failed user update');
            } else {
              console.log(err);
            }
          }
        );

        TradingStats.findOneAndUpdate(
          {
            userId: req.session.passport.user,
            status: 0
          },
          {
            currencySum,
            status,
            success
          },
          { new: true },
          (err, lists) => {
            if (!err) {
              console.log('all good save TransStatus collection updated');
            } else {
              console.log(err);
            }
          }
        );
      });
      //sockets with sale button
      socket.on('saleUser', saleUserData => {
        const { userBalance, userBitcoin } = saleUserData;
        User.findOneAndUpdate(
          {
            _id: req.session.passport.user
          },
          {
            userBalance,
            userBitcoin
          },
          { new: true },
          (err, lists) => {
            if (!err) {
              console.log('SALE! all good save user collection updated');
            } else {
              console.log(err);
            }
          }
        );
      });
      socket.on('saleLog', saleLogData => {
        const { salePrice, bitcoinQuantity } = saleLogData;
        const newTransactionLog = new TradingLog({
          userId: req.session.passport.user,
          //dobavit' user name suda
          sellPrice: salePrice,
          bitcoinQuantity: bitcoinQuantity,
          transactionType: 2
        });
        newTransactionLog
          .save()
          .then(() => {
            console.log('SALE! success save transactionLog');
          })
          .catch(err => console.log(err));
      });
      socket.on('saleStats', saleStatsData => {
        const { salePrice, currencySum } = saleStatsData;
        console.log('saleStatsLog: ', salePrice, currencySum);
      });

      binance.websockets.prevDay('BTCUSDT', (err, res) => {
        io.sockets.emit('price', Number(res.close).toFixed(2));
        // io.sockets.emit('userBalance', cash.userBalance)
      });
    });
    User.findById(req.session.passport.user, (err, obj) => {
      // console.log(obj);
      res.render('trade', {
        list: obj
      });
    });
  });

  return router;
};
