const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');

const middleware = require('../middleware/index');
const { isLoggedIn, isAdmin } = middleware;

const User = require('../models/User');

router.get('/', isLoggedIn, isAdmin, (req, res) => {
  res.render('admin');
});

router.get('/list', (req, res) => {
  User.find((err, docs) => {
    if (!err) {
      res.render('allUsers', {
        list: docs
      });
    } else {
      console.log('Error in retrieving traders list :' + err);
    }
  });
});

router.get('/list/:id', (req, res) => {
  User.findById(req.params.id, (err, obj) => {
    res.render('editInfo', {
      list: obj
    });
  });
});

router.post('/list/editrole', (req, res) => {
  console.log(req.body);
  User.findOneAndUpdate(
    { _id: req.body._id },
    {
      leverage: req.body.leverage,
      role: req.body.radio,
      userBalance: req.body.userBalance
      // userBalance: req.body
    },
    { new: true },
    (err, obj) => {
      if (!err) {
        res.redirect('/admin/list');
      }
    }
  );
});

module.exports = router;
