const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');

const middleware = require('../middleware/index');
const { isLoggedIn, isAdmin } = middleware;

const User = require('../models/User');

router.get('/', (req, res) => {
  res.render('login');
});

router.get('/logout', isLoggedIn, (req, res) => {
  // console.log(req.isAuthenticated());
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/');
});

router.post('/', (req, res, next) => {
  passport.authenticate('local', function(err, user, info) {
    // console.log(user);

    // console.log(req.session.id);
    if (err) {
      return next(err);
    }
    if (!user) {
      req.flash('error_msg', 'there are no user with this data');
      return res.redirect('/');
    }
    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
      if (user.role === 'Admin') {
        return res.redirect('/admin');
      }
      if (user.role === 'Traider') {
        return res.redirect('/trader');
      }
      // return res.redirect('/users/' + user._id);
    });
  })(req, res, next);
});

router.get('/registration', (req, res) => {
  res.render('registration');
});

router.post('/registration', (req, res) => {
  const { name, email, password, password2 } = req.body;
  let errors = [];

  if (!name || !email || !password || !password2) {
    errors.push({ msg: 'Please fill in all fields!' });
  }

  if (password !== password2) {
    errors.push({ msg: 'Password do not match' });
  }

  if (password.length < 6) {
    errors.push({ msg: 'Password should be at least 6 characters' });
  }

  if (errors.length > 0) {
    res.render('registration', {
      errors,
      name,
      email,
      password,
      password2
    });
  } else {
    User.findOne({ email: email }).then(user => {
      if (user) {
        //User exists
        errors.push({ msg: 'Email is already registered' });
        res.render('registration', {
          errors,
          name,
          email,
          password,
          password2
        });
      } else {
        const newUser = new User({
          name,
          email,
          password
        });
        // hash password
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            // Set password to hashed
            newUser.password = hash;
            // Save user
            newUser
              .save()
              .then(user => {
                req.flash(
                  'success_msg',
                  'You are now registred and can log in'
                );
                res.redirect('/');
              })
              .catch(err => console.log(err));
          });
        });
      }
    });
  }
});
module.exports = router;
