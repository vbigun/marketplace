const path = require('path');

// const expressLayouts = require('express-ejs-layouts');
const express = require('express');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require('passport');

const app = express();

//DB config
const db = require('./config/keys').MongoURI;

//passport config
require('./config/passport')(passport);

//connect to mongo
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('mongoDB connected'))
  .catch(err => console.log(err));

mongoose.set('useFindAndModify', false);

//set views EJS
// app.use(expressLayouts);
app.set('view engine', 'ejs');
const publicDirectoryPath = path.join(__dirname, '/public');
app.use(express.static(publicDirectoryPath));

//bodyParser
app.use(express.urlencoded({ extended: false }));

//express session
app.use(
  session({
    secret: 'secret ket',
    resave: true,
    saveUninitialized: true
  })
);

//passport middleware
app.use(passport.initialize());
app.use(passport.session());

//conneeect flash msgg
app.use(flash());

//global vars
app.use((req, res, next) => {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});
const PORT = process.env.PORT || 3000;

let server = app.listen(PORT, () => {
  console.log(`serve GO at http://localhost:${PORT}`);
});
let io = require('socket.io')(server);

//routes
app.use('/', require('./routes/index'));
app.use('/admin', require('./routes/admin'));
app.use('/trader', require('./routes/trader')(io));
app.use('/trading', require('./routes/trading'));
