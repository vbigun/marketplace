const User = require('../models/User');

module.exports = {
  isLoggedIn: function(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    req.flash('error', 'You must be signed in to do that!');
    res.redirect('/');
  },

  isAdmin: function(req, res, next) {
    if (req.user.role === 'Admin') {
      next();
    } else {
      req.flash(
        'error',
        'This site is now read only thanks to spam and trolls.'
      );
      res.redirect('/trader');
    }
  }
};
